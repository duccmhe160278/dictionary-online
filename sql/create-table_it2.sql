create table dictionaries(
id int auto_increment primary key,
`name` nvarchar(50),
`password` varchar(50),
publish bit,
view_by tinyint,
edit_by tinyint,
`description` nvarchar(500),
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50)
);

create table dicts_map_cates(
dict_id int,
cate_id int,
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50),
primary key (dict_id,cate_id)
);

create table dicts_map_words(
dict_id int,
word_id int,
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50),
primary key (dict_id,word_id)
);

create table contributions(
id int auto_increment,
word_id int,
new_definition longtext,
new_syntax longtext,
new_sub_title nvarchar(100),
new_categories varchar(100),
`comment` nvarchar(500),
`status` bit,
notes nvarchar(500),
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50),
primary key (id),
foreign key (created_by) references users(username),
foreign key (updated_by) references users(username),
foreign key (word_id) references words(id)
);

alter table dicts_map_words
add foreign key (word_id) references words(id);

alter table dicts_map_words
add foreign key (dict_id) references dictionaries(id);

alter table dicts_map_cates
add foreign key (cate_id) references categories(id);

alter table dicts_map_cates
add foreign key (dict_id) references dictionaries(id);

alter table dictionaries
add foreign key (created_by) references users(username);
