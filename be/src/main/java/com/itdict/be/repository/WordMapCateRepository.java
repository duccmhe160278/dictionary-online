package com.itdict.be.repository;

import com.itdict.be.entity.DictsMapCates;
import com.itdict.be.entity.WordMapCateId;
import com.itdict.be.entity.WordsMapCategories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WordMapCateRepository extends JpaRepository<WordsMapCategories, WordMapCateId> {
    @Query("SELECT dic FROM WordsMapCategories dic WHERE dic.id.wordId = :id")
    List<WordsMapCategories> getCategory(@Param("id") Integer id);
}
