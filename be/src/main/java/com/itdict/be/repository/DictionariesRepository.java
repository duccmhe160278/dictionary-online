package com.itdict.be.repository;

import com.itdict.be.entity.Dictionaries;
import com.itdict.be.entity.Users;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictionariesRepository extends JpaRepository<Dictionaries, String> {
    @Query("SELECT dic FROM Dictionaries dic WHERE dic.name = :name")
    Dictionaries getDictionariesByName(@Param("name") String name);
    @Query("SELECT dic FROM Dictionaries dic WHERE dic.id = :id")
    Dictionaries getDictionariesById(@Param("id") Integer id);

    @Query("SELECT dic FROM Dictionaries dic WHERE dic.createdBy = :username")
    List<Dictionaries> getAllByUsername(@Param("username") String username);

    @Query("SELECT dic FROM Dictionaries dic WHERE dic.id = :id")
    Dictionaries getAllById(@Param("id") int id);

    @Query("SELECT dic FROM Dictionaries dic WHERE dic.name like :name and dic.viewBy in (1,2)")
    List<Dictionaries> search(@Param("name") String name);

    @Modifying
    @Transactional
    @Query(
            value =
                    "insert into Dictionaries(name, password, view_by, edit_by, description) " +
                            "values (:name, :password, :view_by, :edit_by, :description)",
            nativeQuery = true)
    void save(@Param("name") String name, @Param("password") String password,
    @Param("view_by") Integer view_by, @Param("edit_by") Integer edit_by, @Param("description") String description);
}
