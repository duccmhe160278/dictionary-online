package com.itdict.be.repository;

import com.itdict.be.entity.Words;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface WordRepository extends JpaRepository<Words, Integer> {
    @Query("SELECT w FROM Words w WHERE w.keyword LIKE :keyword and w.publish = true")
    List<Words> searchExactly(@Param("keyword") String keyword);
    @Query("SELECT w FROM Words w WHERE w.subTitle = :sub_title and w.keyword <> :keyword")
    List<Words> searchRelated(Pageable pageable, @Param("sub_title") String subTitle, @Param("keyword") String keyword);

    @Query("SELECT w FROM Words w where w.publish = true")
    List<Words> getTop20(Pageable pageable);


    @Query("SELECT w FROM Words w WHERE w.keyword = :keyword and w.publish = true")
    Words getByKey(@Param("keyword") String keyword);


    @Query("SELECT w FROM Words w WHERE w.id = :keyword")
    Words getByKeywordId(@Param("keyword") Integer keyword);
    @Query("SELECT w FROM Words w WHERE w.id = :id")
    Words getById(@Param("id") Integer id);

    @Query("SELECT w FROM Words w WHERE datediff(:curDate,w.created) <= 7")
    List<Words> getReports(@Param("curDate")Date currentDate);
}
