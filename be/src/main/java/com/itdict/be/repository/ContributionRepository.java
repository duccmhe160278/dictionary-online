package com.itdict.be.repository;

import com.itdict.be.entity.Contributions;
import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.ContributionCountDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContributionRepository extends JpaRepository<Contributions, Integer> {
    @Query("SELECT u FROM Contributions u")
    List<Contributions> searchAll(Pageable pageable);

    @Query("SELECT u FROM Contributions u INNER JOIN  Words WHERE u.word.keyword = :keyword")
    List<Contributions> search(Pageable pageable, @Param("keyword") String keyword);
    @Query("SELECT u FROM Contributions u WHERE u.word.keyword = :keyWord")
    List<Contributions> searchNoPaging(@Param("keyWord") String keyWord);

    @Query("select new com.itdict.be.entity.dto.ContributionCountDto(count(c), c.createdBy) " +
            "from Contributions c group by c.createdBy order by count(c) desc ")
    List<ContributionCountDto> countTop(Pageable pageable);

    @Query("SELECT u FROM Contributions u INNER JOIN  Words " +
            "WHERE u.word.keyword like :keyword and u.createdBy.username = :username")
    List<Contributions> searchWithUsername(Pageable pageable, @Param("keyword") String keyword,
                                           @Param("username") String username);

    @Query("SELECT u FROM Contributions u WHERE u.word.keyword like :keyWord and u.createdBy.username = :username")
    List<Contributions> searchWithUsernameNoPaging(@Param("keyWord") String keyWord,
                                                   @Param("username") String username);
    @Query("SELECT u FROM Contributions u where u.status is null")
    List<Contributions> getList(Pageable pageable);

    @Query("SELECT u FROM Contributions u where month(u.created) = :month")
    List<Contributions> getContributionsByMonth(@Param("month") Integer month);

    @Query("SELECT u FROM Contributions u where u.status = :status")
    List<Contributions> getContributionsByStatus(@Param("status") Boolean status);
}
