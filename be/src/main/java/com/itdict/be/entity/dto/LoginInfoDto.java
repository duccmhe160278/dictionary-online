package com.itdict.be.entity.dto;

import java.util.List;

public class LoginInfoDto {
    private String username;
    private String fullname;
    private List<MenuDto> menu;

    public LoginInfoDto() {
    }

    public LoginInfoDto(String username, String fullname, List<MenuDto> menu) {
        this.username = username;
        this.fullname = fullname;
        this.menu = menu;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public List<MenuDto> getMenu() {
        return menu;
    }

    public void setMenu(List<MenuDto> menu) {
        this.menu = menu;
    }
}
