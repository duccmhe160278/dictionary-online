package com.itdict.be.entity.dto;

import com.itdict.be.entity.Categories;
import com.itdict.be.entity.Words;

import java.util.List;

public class HomeDto {
    private List<Categories> categories;
    private List<Words> words;
    private Integer totalWords;
    private List<ContributionCountDto> counts;

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public List<Words> getWords() {
        return words;
    }

    public void setWords(List<Words> words) {
        this.words = words;
    }

    public Integer getTotalWords() {
        return totalWords;
    }

    public void setTotalWords(Integer totalWords) {
        this.totalWords = totalWords;
    }

    public List<ContributionCountDto> getCounts() {
        return counts;
    }

    public void setCounts(List<ContributionCountDto> counts) {
        this.counts = counts;
    }
}
