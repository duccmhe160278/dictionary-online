package com.itdict.be.entity.dto;

import java.util.List;

public class UserPermissionDto {
    private List<String> approvers;
    private List<String> admins;
    private List<String> inactive;

    public UserPermissionDto() {
    }

    public List<String> getApprovers() {
        return approvers;
    }

    public void setApprovers(List<String> approvers) {
        this.approvers = approvers;
    }

    public List<String> getAdmins() {
        return admins;
    }

    public void setAdmins(List<String> admins) {
        this.admins = admins;
    }

    public List<String> getInactive() {
        return inactive;
    }

    public void setInactive(List<String> inactive) {
        this.inactive = inactive;
    }
}
