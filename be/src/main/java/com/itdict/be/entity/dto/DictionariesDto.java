package com.itdict.be.entity.dto;

import jakarta.annotation.Nullable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.util.Date;
import java.util.List;

public class DictionariesDto {
    private Integer id;
    private String name;
    private String description;
    private List<String> categories;
    private Integer viewBy;
    private Integer editBy;
    private String password;
    private String createdBy;
    @Column(nullable = true)
    private Date created;
    private List<Integer> categoryList;
    private Integer numOfWords;

    public DictionariesDto(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public DictionariesDto(Integer id, String name, String createdBy, Date created, List<String> categoryList) {
        this.id = id;
        this.name = name;
        this.createdBy = createdBy;
        this.created = created;
        this.categories = categoryList;
    }

    public DictionariesDto(Integer id, String name, List<String> categories, Integer viewBy, String createdBy, Date created) {
        this.id = id;
        this.name = name;
        this.categories = categories;
        this.viewBy = viewBy;
        this.createdBy = createdBy;
        this.created = created;
    }

    public DictionariesDto() {
    }

    public Integer getNumOfWords() {
        return numOfWords;
    }

    public void setNumOfWords(Integer numOfWords) {
        this.numOfWords = numOfWords;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<Integer> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Integer> categoryList) {
        this.categoryList = categoryList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {this.name = name;}
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {this.description = description;}

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public Integer getViewBy() {
        return viewBy;
    }

    public void setViewBy(Integer viewBy) {
        this.viewBy = viewBy;
    }

    public Integer getEditBy() {
        return editBy;
    }

    public void setEditBy(Integer editBy) {
        this.editBy = editBy;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
