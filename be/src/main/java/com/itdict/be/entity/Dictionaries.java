package com.itdict.be.entity;

import com.itdict.be.entity.dto.DictionariesDto;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
public class Dictionaries {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String password;
    private Boolean publish;
    private Integer viewBy;
    private Integer editBy;
    private String description;
    @Column(nullable = true)
    private Date created;
    private String createdBy;
    private Date updated;
    private String updatedBy;
    public Dictionaries() {}

    public Dictionaries(String name, String password, Integer view_by, Integer edit_by, String description) {
        this.name = name;
        this.password = password;
        this.viewBy = view_by;
        this.editBy = edit_by;
        this.description = description;
    }

    public Dictionaries(DictionariesDto dto){
        this.name = dto.getName();
        this.description = dto.getDescription();
        this.viewBy = dto.getViewBy();
        this.editBy = dto.getEditBy();
        this.password = dto.getPassword();
        this.createdBy = dto.getCreatedBy();
        this.publish = dto.getViewBy() != 3;
    }

    public Dictionaries(DictionariesDto dto, Integer id){
        this.name = dto.getName();
        this.description = dto.getDescription();
        this.viewBy = dto.getViewBy();
        this.editBy = dto.getEditBy();
        this.password = dto.getPassword();
        this.createdBy = dto.getCreatedBy();
        this.publish = dto.getViewBy() != 3;
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {this.name = name;}
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {this.password = password;}
    public Boolean getPublish() {
        return publish;
    }

    public void setPublish(Boolean publish) {
        this.publish = publish;
    }
    public Integer getViewBy() {
        return viewBy;
    }

    public void setViewBy(Integer view_by) {
        this.viewBy = view_by;
    }
    public Integer getEditBy() {
        return editBy;
    }

    public void setEditBy(Integer edit_by) {
        this.editBy = edit_by;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {this.description = description;}
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String created_by) {
        this.createdBy = created_by;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updated_by) {
        this.updatedBy = updated_by;
    }
}