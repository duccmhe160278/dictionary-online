package com.itdict.be.entity.dto;

import com.itdict.be.entity.Feedbacks;

import java.util.List;

public class FeedbackReportDto {
    private Integer rate;
    private Integer count;
    private List<Feedbacks> feedbacks;

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Feedbacks> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(List<Feedbacks> feedbacks) {
        this.feedbacks = feedbacks;
    }
}
