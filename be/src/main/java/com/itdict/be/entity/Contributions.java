package com.itdict.be.entity;

import jakarta.persistence.*;

import java.util.Date;

@Entity
public class Contributions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "word_id")
    private Words word;

    private String newDefinition;

    private String newSyntax;

    private String newSubTitle;

    private String newCategories;

    private String comment;

    private Boolean status;

    private String notes;

    private Date created;

    @ManyToOne
    @JoinColumn(name = "created_by")
    private Users createdBy;

    private Date updated;

    @ManyToOne
    @JoinColumn(name = "updated_by")
    private Users updatedBy;

    public Contributions() {
    }

    public Contributions(int id, Words word, String newDefinition, String newSyntax
            , String newSubTitle, String newCategories, String comment
            , boolean status, String notes, Date created, Users createdBy
            , Date updated, Users updatedBy) {
        this.id = id;
        this.word = word;
        this.newDefinition = newDefinition;
        this.newSyntax = newSyntax;
        this.newSubTitle = newSubTitle;
        this.newCategories = newCategories;
        this.comment = comment;
        this.status = status;
        this.notes = notes;
        this.created = created;
        this.createdBy = createdBy;
        this.updated = updated;
        this.updatedBy = updatedBy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Words getWord() {
        return word;
    }

    public void setWord(Words word) {
        this.word = word;
    }

    public String getNewDefinition() {
        return newDefinition;
    }

    public void setNewDefinition(String newDefinition) {
        this.newDefinition = newDefinition;
    }

    public String getNewSyntax() {
        return newSyntax;
    }

    public void setNewSyntax(String newSyntax) {
        this.newSyntax = newSyntax;
    }

    public String getNewSubTitle() {
        return newSubTitle;
    }

    public void setNewSubTitle(String newSubTitle) {
        this.newSubTitle = newSubTitle;
    }

    public String getNewCategories() {
        return newCategories;
    }

    public void setNewCategories(String newCategories) {
        this.newCategories = newCategories;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Users getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Users createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Users getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Users updatedBy) {
        this.updatedBy = updatedBy;
    }
}
