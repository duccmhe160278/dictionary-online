package com.itdict.be.entity.dto;

import com.itdict.be.entity.Users;

public class ContributionCountDto {
    private Long counter;

    private String username;

    public ContributionCountDto() {
    }

    public ContributionCountDto(Long counter, Users u) {
        this.counter = counter;
        this.username = u.getUsername();
    }

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
