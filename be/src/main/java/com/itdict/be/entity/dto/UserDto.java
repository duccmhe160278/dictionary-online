package com.itdict.be.entity.dto;

import com.itdict.be.entity.Permissions;
import com.itdict.be.entity.Users;

import java.util.Date;
import java.util.List;

public class UserDto {
    private String username;
    private String email;
    private String phone;
    private String fullname;
    private Date dob;
    private Boolean active;
    private Integer roleId;
    private Integer jobId;
    private String roleName;
    private List<Permissions> menu;

    public List<Permissions> getMenu() {
        return menu;
    }

    public void setMenu(List<Permissions> menu) {
        this.menu = menu;
    }

    public UserDto() {
    }

    public UserDto(String username, String fullname, Integer roleId) {
        this.username = username;
        this.fullname = fullname;
        this.roleId = roleId;
    }

    public UserDto(Users u){
        this.username = u.getUsername();
        this.roleId = u.getRoleId();
        this.fullname = u.getFullname();
        this.email = u.getEmail();
        this.phone = u.getPhone();
        this.jobId = u.getJobId();
        this.dob = u.getDob();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
