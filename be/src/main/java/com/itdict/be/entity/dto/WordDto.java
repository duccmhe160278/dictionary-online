package com.itdict.be.entity.dto;

import com.itdict.be.entity.Categories;

import java.util.List;

public class WordDto {
    private Integer id;
    private String keyword;
    private String defination;
    private String syntax;
    private Boolean publish;
    private String subTitle;
    private List<Categories> categoryList;
    private List<Integer> cateIds;

    private List<String> categories;
    private String createdBy;


    public WordDto() {
    }

    public WordDto(String keyword, String defination, String syntax, Boolean publish, String sub_title) {
        this.keyword = keyword;
        this.defination = defination;
        this.syntax = syntax;
        this.publish = publish;
        this.subTitle = sub_title;
    }

    public WordDto(String keyword, String defination, String syntax, String subTitle, List<Categories> categoryList) {
        this.keyword = keyword;
        this.defination = defination;
        this.syntax = syntax;
        this.subTitle = subTitle;
        this.categoryList = categoryList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDefination() {
        return defination;
    }

    public void setDefination(String defination) {
        this.defination = defination;
    }

    public String getSyntax() {
        return syntax;
    }

    public void setSyntax(String syntax) {
        this.syntax = syntax;
    }

    public Boolean getPublish() {
        return publish;
    }

    public void setPublish(Boolean publish) {
        this.publish = publish;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public List<Categories> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Categories> categoryList) {
        this.categoryList = categoryList;
    }

    public List<Integer> getCateIds() {
        return cateIds;
    }

    public void setCateIds(List<Integer> cateIds) {
        this.cateIds = cateIds;
    }
}
