package com.itdict.be.entity;

import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class DictMapCateId implements Serializable {
    private Integer dictId;
    private Integer cateId;

    public DictMapCateId() {
    }

    public DictMapCateId(Integer dictId, Integer cateId) {
        this.dictId = dictId;
        this.cateId = cateId;
    }

    public Integer getDictId() {
        return this.dictId;
    }

    public void setDictId(Integer dictId) {
        this.dictId = dictId;
    }

    public Integer getCateId() {
        return this.cateId;
    }

    public void setCateId(Integer cateId) {
        this.cateId = cateId;
    }
}
