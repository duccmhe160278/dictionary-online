package com.itdict.be.service.imp;

import com.itdict.be.entity.*;
import com.itdict.be.entity.dto.*;
import com.itdict.be.repository.*;
import com.itdict.be.service.ContributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class ContributionServiceImpl implements ContributionService {

    @Autowired
    private ContributionRepository contributionRepository;
    @Autowired
    private WordRepository wordRepository;

    @Autowired
    private WordMapCateRepository wordMapCateRepository;
    @Autowired
    private CategoriesRepository categoriesRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Contributions> getAll() {
        return contributionRepository.findAll();
    }

    @Override
    public List<ContributionsDto> search(Integer currentPage, String keyword, String username) {
        List<Contributions> contributions = new ArrayList<>();
        List<ContributionsDto> contributionsDtos = new ArrayList<>();
        if (keyword.trim().isEmpty() && username.trim().isEmpty()) {
            contributions = contributionRepository.searchAll(
                    PageRequest.of(currentPage - 1, 10, Sort.by(Sort.Direction.DESC, "id"))
            );
        } else {
            if (username.trim().isEmpty()) {
                contributions = contributionRepository.search(
                        PageRequest.of(currentPage - 1, 10,
                                Sort.by(Sort.Direction.DESC, "id")), keyword);

            } else {
                contributions = contributionRepository.searchWithUsername(
                        PageRequest.of(currentPage - 1, 10,
                                Sort.by(Sort.Direction.DESC, "id")), "%" + keyword + "%", username);
            }
        }
        return getContributionsDtos(contributions, contributionsDtos);
    }

    public List<Categories> getCategories(List<WordsMapCategories> map) {
        List<Categories> returnList = new ArrayList<>();
        if (map.size() > 0) {
            for (WordsMapCategories w : map) {
                Categories c = categoriesRepository.findById(w.getId().getCategoryId()).get();
                if (c != null) {
                    returnList.add(c);
                }
            }
        }
        return returnList.size() > 0 ? returnList : null;

    }

    @Override
    public Integer countTotal(String keyWord, String username) {
        List<Contributions> contributions;
        if (keyWord.trim().isEmpty() && username.trim().isEmpty()) {
            contributions = contributionRepository.findAll();
        } else {
            if (username.trim().isEmpty()) {
                contributions = contributionRepository.searchNoPaging(keyWord);
            } else {
                contributions = contributionRepository.searchWithUsernameNoPaging("%" + keyWord + "%", username);
            }
        }
        return contributions != null ? contributions.size() : 0;
    }

    @Override
    public ContributionsDto approve(ContributionsDto dto) {
        // update Approve on table Contributions
        Contributions c = contributionRepository.findById(dto.getId()).get();
        c.setUpdatedBy(userRepository.findById(dto.getUpdatedBy()).get());
        c.setUpdated(new Date());
        c.setStatus(true);
        contributionRepository.save(c);

        // update Word
        Words w = wordRepository.getByKeywordId(dto.getWordId());
        w.setPublish(true);
        w.setActive(true);
        w.setDefination(dto.getNewWord().getDefination());
        w.setSyntax(dto.getNewWord().getSyntax());
        w.setSub_title(dto.getNewWord().getSubTitle());
        w.setUpdatedBy(dto.getUpdatedBy());
        w.setUpdated(new Date());
        wordRepository.save(w);


        //update Category
        if (dto.getOrgWord().getCategoryList() != null) {
            for (Categories cate : dto.getOrgWord().getCategoryList()) {
                WordMapCateId wmcId = new WordMapCateId(dto.getWordId(), cate.getId());
                WordsMapCategories wordsMapCategories = new WordsMapCategories(wmcId);
                wordMapCateRepository.delete(wordsMapCategories);
            }
        }

        if (dto.getNewWord().getCategoryList() != null) {
            for (Categories ca : dto.getNewWord().getCategoryList()) {
                WordMapCateId wmcId = new WordMapCateId(dto.getWordId(), ca.getId());
                WordsMapCategories wordsMapCategories = new WordsMapCategories(wmcId);
                wordMapCateRepository.save(wordsMapCategories);
            }
        }

        return dto;
    }

    @Override
    public Contributions findById(Integer id) {
        return contributionRepository.findById(id).get();
    }

    @Override
    public void save(Contributions c) {
        contributionRepository.save(c);
    }

    @Override
    public List<ContributionCountDto> countTop() {
        return contributionRepository.countTop(PageRequest.of(0, 10));
    }

    @Override
    public void delete(Integer id) {
        contributionRepository.deleteById(id);
    }

    @Override
    public List<ContributionsDto> getList() {
        List<Contributions> contributions = new ArrayList<>();
        List<ContributionsDto> contributionsDtos = new ArrayList<>();
        contributions = contributionRepository.getList(
                PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "id"))
        );
        return getContributionsDtos(contributions, contributionsDtos);
    }

    @Override
    public Contributions addContribution(Contributions c) {
        return contributionRepository.save(c);
    }

    @Override
    public ContributionsDto getById(Integer id) {
        Contributions con = contributionRepository.findById(id).get();
        return getContributionDto(con);
    }

    @Override
    public SystemReportResponse getReport(SystemReportResponse res) {
        int total = 0;
        List<Contributions> list = contributionRepository.findAll();
        total = list.isEmpty()? 0 : list.size();
        res.setTotalContribution(total);
        list = contributionRepository.getContributionsByMonth(new Date().getMonth());
        res.setContributionInMonth(list.isEmpty()? 0 : list.size());
        list = contributionRepository.getContributionsByStatus(true);
        int count = list.isEmpty()? 0 : list.size();
        double d = (count * 100)/total;
        res.setApprovedRate(d);
        list = contributionRepository.getContributionsByStatus(false);
        count = list.isEmpty()? 0 : list.size();
        d = (count * 100)/total;
        res.setRejectedRate(d);
        return res;
    }

    private List<ContributionsDto> getContributionsDtos(List<Contributions> contributions, List<ContributionsDto> contributionsDtos) {
        if (contributions.size() > 0) {
            for (Contributions con : contributions) {
                contributionsDtos.add(getContributionDto(con));
            }
        }
        return contributionsDtos;
    }

    private ContributionsDto getContributionDto(Contributions con){
        ContributionsDto contributionsDto = new ContributionsDto();
        contributionsDto.setId(con.getId());
        contributionsDto.setKeyword(con.getWord().getKeyword());
        contributionsDto.setStatus(con.getStatus());
        contributionsDto.setComment(con.getComment());
        contributionsDto.setCreatedBy(con.getCreatedBy().getUsername());
        contributionsDto.setWordId(con.getWord().getId());
//                contributionsDto.set(con.getNewSubTitle());
        Words orW = wordRepository.getByKeywordId(con.getWord().getId());

        List<WordsMapCategories> orMap = wordMapCateRepository.getCategory(orW.getId());
        List<Categories> c = getCategories(orMap);
        WordDto orgWord = new WordDto();
        orgWord.setKeyword(orW.getKeyword());
        orgWord.setSubTitle(orW.getSubTitle());
        orgWord.setCategoryList(c);
        orgWord.setDefination(orW.getDefination());
        orgWord.setSyntax(orW.getSyntax());
        contributionsDto.setOrgWord(orgWord);


        // set new word
        List<WordsMapCategories> newMap = new ArrayList<>();
        if (!con.getNewCategories().isEmpty()) {
            List<String> items = Arrays.asList(con.getNewCategories().split("\\s*;\\s*"));
            for (String i : items) {
                if (!i.trim().isEmpty()) {
                    Integer id = Integer.parseInt(i.trim());
                    WordsMapCategories wMC = new WordsMapCategories();
                    WordMapCateId mId = new WordMapCateId();
                    mId.setCategoryId(id);
                    wMC.setId(mId);
                    newMap.add(wMC);
                }
            }
        }
        List<Categories> nC = getCategories(newMap);
        WordDto newWord = new WordDto(
                con.getWord().getKeyword(), con.getNewDefinition(), con.getNewSyntax(), con.getNewSubTitle(), nC
        );
        contributionsDto.setNewWord(newWord);
        contributionsDto.setContact(con.getCreatedBy().getEmail() != null
                ? con.getCreatedBy().getEmail() : con.getCreatedBy().getPhone());
        contributionsDto.setNotes(con.getNotes());
        if (con.getUpdatedBy() != null) {
            contributionsDto.setUpdatedBy(con.getUpdatedBy().getUsername());
            contributionsDto.setApproverContact(
                    con.getUpdatedBy().getEmail() != null
                            ? con.getUpdatedBy().getEmail() : con.getUpdatedBy().getPhone()
            );
        }
        return contributionsDto;
    }

}
