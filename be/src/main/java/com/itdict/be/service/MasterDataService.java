package com.itdict.be.service;

import com.itdict.be.entity.Permissions;
import com.itdict.be.entity.dto.MasterDataResponse;

import java.util.List;

public interface MasterDataService {
    public MasterDataResponse init();
    public List<String> getPermissionKeys();

}
