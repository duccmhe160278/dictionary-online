package com.itdict.be.service;

import com.itdict.be.entity.Dictionaries;
import com.itdict.be.entity.DictsMapCates;
import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.DictionariesDto;

import java.util.List;

public interface DictionariesService {
    public Dictionaries save(Dictionaries dictionaries);
    public Dictionaries getByName(String name);
    public void saveMapCate(DictsMapCates map);
    public List<DictionariesDto> getAllByUsername(String username);
    public List<DictionariesDto> search(String name);
    Dictionaries getById(Integer id);
    public Dictionaries update(DictionariesDto dictionaries);
}
