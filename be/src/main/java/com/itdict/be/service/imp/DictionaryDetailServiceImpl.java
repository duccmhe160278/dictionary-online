package com.itdict.be.service.imp;

import com.itdict.be.entity.DictsMapWords;
import com.itdict.be.entity.Dictionaries;
import com.itdict.be.entity.DictsMapCates;
import com.itdict.be.entity.Words;
import com.itdict.be.repository.*;
import com.itdict.be.service.DictionaryDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DictionaryDetailServiceImpl implements DictionaryDetailService {
    @Autowired
    private DictionaryDetailRepository dictionaryDetailRepository;

    @Autowired
    private DictionariesRepository dictionariesRepository;
    @Autowired
    private DictMapCateRepository dictMapCateRepository;

    @Override
    public List<DictsMapWords> getList(Integer dictionaryId) {
        return dictionaryDetailRepository.getList(dictionaryId);
    }

    @Override
    public void update(List<DictsMapWords> dictsMapWords) {
        for (DictsMapWords w : dictsMapWords) {
            dictionaryDetailRepository.save(w);
        }
    }

    @Override
    public void deleteWord(Integer id) {
        dictionaryDetailRepository.deleteById(id);
    }

    @Override
    public Dictionaries clone(Integer dictionaryId, String currentUser) {
        Dictionaries orgDictionary = dictionariesRepository.getDictionariesById(dictionaryId);
        List<DictsMapWords> list = getList(dictionaryId);

        // clone
        Dictionaries cloneDict = new Dictionaries();
        cloneDict.setCreated(new Date());
        cloneDict.setCreatedBy(currentUser);
        cloneDict.setName(orgDictionary.getName() + "_Clone");
        cloneDict.setPublish(false);
        cloneDict.setViewBy(3);
        cloneDict.setEditBy(3);

        cloneDict = dictionariesRepository.save(cloneDict);

        // clone word
        for(DictsMapWords dmw : list){
            DictsMapWords d = new DictsMapWords();
            d.setDictionaries(cloneDict);
            d.setWord(dmw.getWord());
            d.setKeyword(dmw.getKeyword());
            d.setDefinition(dmw.getDefinition());
            dictionaryDetailRepository.save(d);
        }
        return cloneDict;
    }

    @Override
    public void deleteDict(Integer dictId) {
        Dictionaries d = dictionariesRepository.getDictionariesById(dictId);
        List<DictsMapWords> list = getList(dictId);
        List<DictsMapCates> mapCate = dictMapCateRepository.getListCateByDict(dictId);

        for(DictsMapWords dw : list){
            dictionaryDetailRepository.delete(dw);
        }

        for(DictsMapCates m :mapCate){
            dictMapCateRepository.delete(m);
        }

        dictionariesRepository.delete(d);
    }

    @Override
    public Dictionaries getById(Integer id) {
        return dictionariesRepository.getDictionariesById(id);
    }


}
