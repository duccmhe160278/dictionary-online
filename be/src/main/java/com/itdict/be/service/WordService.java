package com.itdict.be.service;

import com.itdict.be.entity.Words;
import com.itdict.be.entity.dto.SystemReportResponse;
import com.itdict.be.entity.dto.WordDto;

import java.util.List;

public interface WordService {

    public List<WordDto> searchRelated(String subTitle,String keyword);

    public List<WordDto> searchExactly(String keyword);
    public List<Words> getTop20();
    public Integer count();
    public Words addWord(Words w);
    public Words getByKey(String key);

    Words getById(Integer wordId);

    boolean saveWord(Words words);
    public void save(WordDto wordDto);
    public SystemReportResponse getReport(SystemReportResponse response);
}
