package com.itdict.be.service.imp;

import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.SystemReportResponse;
import com.itdict.be.entity.dto.UserDto;
import com.itdict.be.entity.dto.UserPermissionDto;
import com.itdict.be.repository.RoleRepository;
import com.itdict.be.repository.UserRepository;
import com.itdict.be.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<Users> getAll() {
        return userRepository.findAll();
    }

    @Override
    public UserDto getLoginInfo(String username, String password) {
        Users us = userRepository.login(username, password);
        if (us != null) {
            return new UserDto(us.getUsername(), us.getFullname(), us.getRoleId());
        }
        return null;
    }

    @Override
    public List<UserDto> search(Integer currentPage, String username) {
        List<Users> users;
        List<UserDto> rs = new ArrayList<>();
        if (username.trim().isEmpty()) {
            users = userRepository.searchAll(PageRequest.of(currentPage - 1, 10));
        } else {
            users = userRepository.search(PageRequest.of(currentPage - 1, 10), username);
        }
        if (users != null) {
            for (Users u : users) {
                UserDto userDto = new UserDto();
                userDto.setUsername(u.getUsername());
                userDto.setEmail(u.getEmail());
                userDto.setActive(u.getActive());
                userDto.setPhone(u.getPhone());
                userDto.setRoleId(u.getRoleId());
                userDto.setRoleName(roleRepository.getReferenceById(u.getRoleId()).getName());
                rs.add(userDto);
            }
        }
        return rs;
    }

    @Override
    public Integer countTotal(String username) {
        List<Users> users;
        if (username.trim().isEmpty()) {
            users = userRepository.findAll();
        } else {
            users = userRepository.searchNoPaging(username);
        }
        return users != null ? users.size() : 0;
    }

    @Override
    public void delete(String username) {
        userRepository.deleteById(username);
    }

    @Override
    public void save(Users u) {
        userRepository.save(u);
    }

    @Override
    public Users getByUsername(String username) {
        return userRepository.getUsersByUsername(username);
    }

    private String randomString(int n) {

        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            int index = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString.charAt(index));
        }

        return sb.toString();
    }

    @Override
    public Users forgotPassword(Users u) {
        Users updateUser = userRepository.getUsersByUsername(u.getUsername());
        if(updateUser != null){
            String newPassword = randomString(8);
            updateUser.setPassword(newPassword);
            userRepository.save(updateUser);
        }
        return updateUser;
    }

    @Override
    public UserDto resetPassword(Users u) {
        Users updateUser = userRepository.getUsersByUsername(u.getUsername());
        if(updateUser != null){
            updateUser.setPassword(u.getPassword());
            userRepository.save(updateUser);
            return new UserDto(updateUser.getUsername(), updateUser.getPassword(), updateUser.getRoleId());
        }
        return null;
    }

    @Override
    public UserPermissionDto getUserPermission() {
        UserPermissionDto res = new UserPermissionDto();
        res.setAdmins(userRepository.getUsersByRole("Administrator"));
        res.setApprovers(userRepository.getUsersByRole("Approver"));
        res.setInactive(userRepository.getInactiveUsers());
        return res;
    }

    @Override
    public List<String> search(String us, String role) {
        return userRepository.searchUsers(role,"%" + us + "%", PageRequest.of(0, 10));
    }

    @Override
    public SystemReportResponse getUserReport() {
        SystemReportResponse response = new SystemReportResponse();
        List<Users> list = userRepository.findAll();
        response.setTotalUser(list.isEmpty()? 0 : list.size());
        list = userRepository.getInactiveReport(new Date());
        response.setInactiveUser(list.isEmpty()? 0 : list.size());
        list = userRepository.getNewReport(new Date());
        response.setNewUser(list.isEmpty()? 0 : list.size());
        return response;
    }
}
