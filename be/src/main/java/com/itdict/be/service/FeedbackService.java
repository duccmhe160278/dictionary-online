package com.itdict.be.service;

import com.itdict.be.entity.Dictionaries;
import com.itdict.be.entity.Feedbacks;
import com.itdict.be.entity.dto.FeedbackReportDto;
import com.itdict.be.entity.dto.SystemReportResponse;

import java.util.List;

public interface FeedbackService {
    public Feedbacks save(Feedbacks feedbacks);

    List<Feedbacks> getAllFeedbacks(Integer rate);
    List<FeedbackReportDto> getReport();
    SystemReportResponse getSystemReport(SystemReportResponse res);
}
