package com.itdict.be.service.imp;

import com.itdict.be.entity.Feedbacks;
import com.itdict.be.entity.dto.FeedbackReportDto;
import com.itdict.be.entity.dto.SystemReportResponse;
import com.itdict.be.repository.FeedbackRepository;
import com.itdict.be.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Indexed;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class FeedbackServiceImpl implements FeedbackService {
    @Autowired
    private FeedbackRepository feedbackRepository;
    @Override
    public Feedbacks save(Feedbacks feedbacks) {
        feedbacks.setCreated(new Date(System.currentTimeMillis()));
        return feedbackRepository.save(feedbacks);
    }

    @Override
    public List<Feedbacks> getAllFeedbacks(Integer rate) {
        List<Feedbacks> feedbacks;
        if(rate == 0){
            feedbacks = feedbackRepository.findAll(Sort.by(Sort.Direction.DESC, "created"));
        }else{
            feedbacks = feedbackRepository.getReport(rate);
        }
        return feedbacks;
    }

    @Override
    public List<FeedbackReportDto> getReport() {
        List<FeedbackReportDto> feedbackReportDtoList = new ArrayList<>();
        List<Feedbacks> all = feedbackRepository.findAll();
        FeedbackReportDto allDto = new FeedbackReportDto();
        allDto.setRate(0);
        allDto.setCount(!all.isEmpty()? all.size() : 0);
        feedbackReportDtoList.add(allDto);
        for(Integer i = 1; i <= 5; i++){
            FeedbackReportDto dto = new FeedbackReportDto();
            dto.setRate(i);
            List<Feedbacks> feedbacks = feedbackRepository.getReport(i);
            dto.setCount(!feedbacks.isEmpty()? feedbacks.size() : 0);
            feedbackReportDtoList.add(dto);
        }
        return feedbackReportDtoList;
    }

    @Override
    public SystemReportResponse getSystemReport(SystemReportResponse res) {
        Integer[] rate = new Integer[]{3,4,5};
        List<Feedbacks> list = feedbackRepository.getSystemReport(Arrays.stream(rate).toList());
        res.setLike(list.isEmpty()? 0 : list.size());
        rate = new Integer[]{1,2};
        list = feedbackRepository.getSystemReport(Arrays.stream(rate).toList());
        res.setDislike(list.isEmpty()? 0 : list.size());
        return res;
    }


}
