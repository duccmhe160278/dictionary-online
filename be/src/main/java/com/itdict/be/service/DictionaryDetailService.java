package com.itdict.be.service;

import com.itdict.be.entity.DictsMapWords;
import com.itdict.be.entity.Dictionaries;
import com.itdict.be.entity.DictsMapCates;
import com.itdict.be.entity.Words;

import java.util.List;

public interface DictionaryDetailService {
    public List<DictsMapWords> getList(Integer dictionaryId);
    public void update(List<DictsMapWords> dictsMapWords);
    public void deleteWord(Integer id);
    public Dictionaries clone(Integer dictionaryId, String currentUser);
    public void deleteDict(Integer dictId);
    public Dictionaries getById(Integer id);
}
