package com.itdict.be.service.imp;

import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.UserDto;
import com.itdict.be.repository.RoleRepository;
import com.itdict.be.repository.UpdateProfileRepository;
import com.itdict.be.service.UpdateProfileService;
import com.itdict.be.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UpdateProfileServiceImpl implements UpdateProfileService {
    @Autowired
    private UpdateProfileRepository updateProfileRepository;

    @Override
    public Users getUser(String username) {
        return updateProfileRepository.getUser(username);
    }

    @Override
    public void save(Users user) {updateProfileRepository.save(user);}
}
