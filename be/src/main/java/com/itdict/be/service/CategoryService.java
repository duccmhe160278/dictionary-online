package com.itdict.be.service;

import com.itdict.be.entity.Categories;

import java.util.List;

public interface CategoryService {
    public List<Categories> getAll();
    public void create(Categories category);
    public void delete(Integer id);
    public Categories getByName(String name);
}
