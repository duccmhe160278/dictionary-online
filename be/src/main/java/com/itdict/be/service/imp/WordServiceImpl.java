package com.itdict.be.service.imp;

import com.itdict.be.entity.Categories;
import com.itdict.be.entity.WordMapCateId;
import com.itdict.be.entity.Words;
import com.itdict.be.entity.WordsMapCategories;
import com.itdict.be.entity.dto.SystemReportResponse;
import com.itdict.be.entity.dto.WordDto;
import com.itdict.be.repository.CategoriesRepository;
import com.itdict.be.repository.WordMapCateRepository;
import com.itdict.be.repository.WordRepository;
import com.itdict.be.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class WordServiceImpl implements WordService {

    @Autowired
    private WordRepository wordRepository;
    @Autowired
    private WordMapCateRepository wordMapCateRepository;
    @Autowired
    private CategoriesRepository categoriesRepository;

    @Override
    public List<WordDto> searchRelated(String subTitle, String keyword) {
        List<Words> words;
        List<WordDto> wd = new ArrayList<>();
        words = wordRepository.searchRelated(PageRequest.of(0, 3), subTitle, keyword);
        return getWordDtos(words, wd);
    }

    @Override
    public List<WordDto> searchExactly(String keyword) {
        List<Words> words;
        List<WordDto> wd = new ArrayList<>();
        words = wordRepository.searchExactly(keyword);

        return getWordDtos(words, wd);
    }

    @Override
    public List<Words> getTop20() {
        return wordRepository.getTop20(PageRequest.of(0, 20, Sort.by(Sort.Direction.DESC, "times")));
    }

    @Override
    public Integer count() {
        return wordRepository.findAll() != null ? wordRepository.findAll().size() : 0;
    }

    @Override
    public Words addWord(Words w) {
        return wordRepository.save(w);
    }

    @Override
    public Words getByKey(String key) {
        return wordRepository.getByKey(key);
    }

    @Override
    public Words getById(Integer wordId) {
        return wordRepository.getById(wordId);
    }

    @Override
    public boolean saveWord(Words words) {
        try {
            wordRepository.save(words);
            return true;
        }catch (Exception exception){
            exception.getMessage();
        }
        return false;
    }

    @Override
    public void save(WordDto wordDto) {
        Words w = new Words();
        w.setKeyword(wordDto.getKeyword());
        w.setDefination(wordDto.getDefination());
        w.setSyntax(wordDto.getSyntax());
        w.setSub_title(wordDto.getSubTitle());
        w.setActive(true);
        w.setPublish(true);
        w.setTimes(0);
        w.setCreatedBy(wordDto.getCreatedBy());
        w.setCreated(new Date());

        List<Categories> categories = new ArrayList<>();
        if(!wordDto.getCategories().isEmpty()){
            for(String cate : wordDto.getCategories()){
                Categories ca = categoriesRepository.getByName(cate);
                if(ca != null){
                    categories.add(ca);
                }
            }
        }

        Words saveEntity = wordRepository.save(w);
        for (Categories c: categories){
            WordMapCateId wmcId = new WordMapCateId(saveEntity.getId(), c.getId());
            WordsMapCategories wordsMapCategories = new WordsMapCategories(wmcId);
            wordMapCateRepository.save(wordsMapCategories);
        }
    }

    @Override
    public SystemReportResponse getReport(SystemReportResponse response) {
        List<Words> list = wordRepository.findAll();
        response.setTotalWord(list.isEmpty()? 0 : list.size());
        list = wordRepository.getReports(new Date());
        response.setNewWord(list.isEmpty()? 0 : list.size());
        return response;
    }

    private List<WordDto> getWordDtos(List<Words> words, List<WordDto> wd) {
        if (!words.isEmpty()) {
            for (Words w : words) {
                WordDto wordDto = new WordDto();
                wordDto.setKeyword(w.getKeyword());
                wordDto.setDefination(w.getDefination());
                wordDto.setSubTitle(w.getSubTitle());
                wordDto.setSyntax(w.getSyntax());
                wordDto.setPublish(w.getPublish());
                wordDto.setId(w.getId());
                List<WordsMapCategories> map = wordMapCateRepository.getCategory(w.getId());
                if (!map.isEmpty()) {
                    List<Integer> cateIds = new ArrayList<>();
                    for (WordsMapCategories item : map) {
                        cateIds.add(item.getId().getCategoryId());
                    }
                    wordDto.setCateIds(cateIds);
                }
                wd.add(wordDto);
            }
        }
        return wd;
    }
}
