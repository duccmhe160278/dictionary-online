package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.*;
import com.itdict.be.service.DictionariesService;
import com.itdict.be.service.DictionaryDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/dictionary-detail")
public class DictionaryDetailRestAPI {
    @Autowired
    private DictionaryDetailService dictionaryDetailService;
    @GetMapping("/get")
    @CrossOrigin
    public ResponseEntity<Response<DictsMapWords>> getWordFromDict(@RequestParam Integer dictionaryId) {
        Response<DictsMapWords> response = new Response<>();
        try {
            response.setStatus(200);
            response.setList(dictionaryDetailService.getList(dictionaryId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/clone")
    @CrossOrigin
    public ResponseEntity<Response<Dictionaries>> cloneDictionary(@RequestParam Integer id, @RequestParam String user) {
        Response<Dictionaries> response = new Response<>();
        try {
            Dictionaries rs = dictionaryDetailService.clone(id,user);
            response.setData(rs);
            response.setStatus(200);
            response.setMessage("Clone dictionary successfully.");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/delete")
    @CrossOrigin
    public ResponseEntity<Response<Object>> deleteDictionary(@RequestParam Integer id) {
        Response<Object> response = new Response<>();
        try {
            dictionaryDetailService.deleteDict(id);
            response.setStatus(200);
            response.setMessage("Remove dictionary successfully.");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update-detail")
    @CrossOrigin
    public ResponseEntity<Response<Object>> updateWords(@RequestBody List<DictsMapWords> words) {
        Response<Object> response = new Response<>();
        try {
            dictionaryDetailService.update(words);
            response.setStatus(200);
            response.setMessage("Update successfully.");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/delete-word")
    @CrossOrigin
    public ResponseEntity<Response<Object>> delete(@RequestParam Integer id) {
        Response<Object> response = new Response<>();
        try {
            dictionaryDetailService.deleteWord(id);
            response.setStatus(200);
            response.setMessage("Delete successfully.");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/access-dict")
    @CrossOrigin
    public ResponseEntity<Response<Object>> checkAccess(@RequestParam Integer id,
                                                        @RequestParam String password) {
        Response<Object> response = new Response<>();
        try {
            // == code in here
            Dictionaries dictionaries = dictionaryDetailService.getById(id);
            if(password.equals(dictionaries.getPassword())){
                response.setStatus(200);
            }
            else {
                response.setStatus(500);
                response.setMessage("Password is not match!");
            }
            //
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
