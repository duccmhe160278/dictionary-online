package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Jobs;
import com.itdict.be.entity.Permissions;
import com.itdict.be.entity.dto.MasterDataResponse;
import com.itdict.be.service.MasterDataService;
import com.itdict.be.service.PermissionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/master-data")
public class MasterDataRestAPI {
    @Autowired
    private MasterDataService masterDataService;
    @Autowired
    private PermissionsService permissionsService;
    @GetMapping("/init")
    @CrossOrigin
    public ResponseEntity<Response<MasterDataResponse>> init() {
        Response<MasterDataResponse> response = new Response<>();
        try{
            response.setStatus(200);
            response.setData(masterDataService.init());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getPermissionKeys")
    @CrossOrigin
    public ResponseEntity<Response<String>> getPermissionKeys() {
        Response<String> response = new Response<>();
        try{
            response.setStatus(200);
            response.setList(masterDataService.getPermissionKeys());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/savePermission")
    @CrossOrigin
    public ResponseEntity<Response<Object>> savePermission(@RequestBody Permissions permission){
        Response<Object> response = new Response<>();
        try{
            if(permission.getId() == null){
                if(!permissionsService.checkExist(permission)){
                    permission.setCreatedBy("system.admin");
                    permission.setCreated(new Date());
                    permission.setUpdatedBy("system.admin");
                    permission.setUpdated(new Date());
                    permissionsService.save(permission);
                    response.setStatus(200);
                    response.setMessage("Save screen permission successfully.");
                }else{
                    response.setStatus(500);
                    response.setMessage("Screen permission is existed.");
                }
            }else{
                permission.setValue("");
                permission.setUpdatedBy("system.admin");
                permission.setUpdated(new Date());
                permissionsService.save(permission);
                response.setStatus(200);
                response.setMessage("Save screen permission successfully.");
            }

            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/deletePermission")
    @CrossOrigin
    public ResponseEntity<Response<Object>> delete(@RequestParam Integer id) {
        Response<Object> response = new Response<>();
        try{
            permissionsService.delete(id);
            response.setStatus(200);
            response.setMessage("Delete successfully;");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
