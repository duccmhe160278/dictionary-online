package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Feedbacks;
import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.HomeDto;
import com.itdict.be.entity.dto.SystemReportResponse;
import com.itdict.be.entity.dto.UserDto;
import com.itdict.be.service.*;
import org.apache.xmlbeans.impl.xb.xsdschema.Attribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/home")
public class HomeRestAPI {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private WordService wordService;
    @Autowired
    private ContributionService contributionService;
    @Autowired
    private UserService userService;
    @Autowired
    private FeedbackService feedbackService;

    @GetMapping("/init")
    @CrossOrigin
    public ResponseEntity<Response<HomeDto>> init() {
        Response<HomeDto> response = new Response<>();
        try{
            HomeDto homeDto = new HomeDto();
            homeDto.setCategories(categoryService.getAll());
            homeDto.setWords(wordService.getTop20());
            homeDto.setTotalWords(wordService.count());
            homeDto.setCounts(contributionService.countTop());
            response.setStatus(200);
            response.setData(homeDto);
            return  new ResponseEntity<>(response,HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get-report")
    @CrossOrigin
    public ResponseEntity<Response<SystemReportResponse>> getReport(){
        Response<SystemReportResponse> response = new Response<>();
        try{
            SystemReportResponse rs = new SystemReportResponse();
            //=== code function in here
            rs = userService.getUserReport();
            rs = wordService.getReport(rs);
            rs = feedbackService.getSystemReport(rs);
            rs = contributionService.getReport(rs);
            //===
            response.setStatus(200);
            response.setData(rs);
            return  new ResponseEntity<>(response,HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
