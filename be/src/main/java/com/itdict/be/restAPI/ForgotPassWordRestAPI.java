package com.itdict.be.restAPI;

//import org.springframework.web.bind.annotation.RequestMapping;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Users;
import com.itdict.be.service.UserService;
import jdk.dynalink.beans.StaticClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/forgotPassword/")
public class ForgotPassWordRestAPI {
    @Autowired
    private UserService userService;

    @PostMapping ("/forgot")
    @CrossOrigin
    public ResponseEntity<Response<Users>> forgot(@RequestBody Users u) {
        Response<Users> response = new Response<>();
        try{
            if(u.getUsername().isEmpty()){
                response.setStatus(500);
                response.setMessage("Username is required.");
            }
            else {
                if (u.getPhone().isEmpty() && u.getEmail().isEmpty()) {
                    response.setStatus(500);
                    response.setMessage("Need phone or email for validate account.");
                } else {
                    Users us = userService.getByUsername(u.getUsername());
                    if (us == null) {
                        response.setStatus(500);
                        response.setMessage("Username is not exist.");
                    } else {
                        if (us.getActive() == false) {
                            response.setStatus(500);
                            response.setMessage("Username is inactive. Please contact to admin [admin@gmail.com]");
                        } else {
                            boolean isValid = true;
                            String mes = "";
                            if(!u.getEmail().isEmpty()){
                                if(us.getEmail().isEmpty()){
                                    isValid = false;
                                    mes += "Email is not match. ";
                                }else {
                                    if (!u.getEmail().equals(us.getEmail())) {
                                        isValid = false;
                                        mes += "Email is not match. ";
                                    }
                                }
                            }
                            if(!u.getPhone().isEmpty()){
                                if(us.getPhone().isEmpty()){
                                    isValid = false;
                                    mes += "Phone is not match. ";
                                }else {
                                    if (!u.getPhone().equals(us.getPhone())) {
                                        isValid = false;
                                        mes += "Phone is not match. ";
                                    }
                                }
                            }
                            if(isValid){
                                response.setData(userService.forgotPassword(u));
                                response.setStatus(200);
                            }else {
                                response.setStatus(500);
                                response.setMessage(mes);
                            }
                        }
                    }
                }
            }
//                if (!username.equals("")){
//                    if(username.equals(us.getUsername())){
//                        if ( !phone.equals("") && !email.equals("")){
//                            if ( phone.equals(us.getPhone()) ||email.equals(us.getEmail())){
//                                userService.getInputUser(username);
//                                userService.ForgotPassWord(username);
//                                response.setStatus(200);
//                                response.setData(us);
//                            }else{
//                                msg = "Cant find!";
//                                response.setStatus(201);
//                                response.setMessage(msg);
//                            }
//                        }else {
//                            msg = "Please fill 1 of this!";
//                            response.setStatus(201);
//                            response.setMessage(msg);
//                        }
//                    }else {
//                        response.setStatus(201);
//                        response.setMessage("cant find!");
//                    }
//                }else {
//                    msgUsername = "Please not empty!";
//                    response.setStatus(201);
//                    response.setMessage(msgUsername);
//                }
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}