package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.*;
import com.itdict.be.entity.dto.DictionariesDto;
import com.itdict.be.service.DictionariesService;
import com.itdict.be.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/dictionary")
public class DictionaryRestAPI {
    @Autowired
    private DictionariesService dictionariesService;

    @PostMapping("/create")
    @CrossOrigin
    public ResponseEntity<Response<Object>> create(@RequestBody DictionariesDto dictionary) {
        Response<Object> response = new Response<>();
        try {
            if (dictionary.getName() != null) {
                Dictionaries d = dictionariesService.save(new Dictionaries(dictionary));
                if(dictionary.getCategoryList() != null){
                    if (dictionary.getCategoryList().size() > 0) {
                        for (Integer cName : dictionary.getCategoryList()) {
                            DictsMapCates m = new DictsMapCates();
                            DictMapCateId id = new DictMapCateId();
                            id.setDictId(d.getId());
                            id.setCateId(cName);
                            m.setId(id);
                            dictionariesService.saveMapCate(m);
                        }
                    }
                }

                response.setStatus(200);
                response.setMessage("Create new dictionary successfully.");
            } else {
                response.setStatus(500);
                response.setMessage("Dictionary name is required.");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getAllByUsername")
    @CrossOrigin
    public ResponseEntity<Response<DictionariesDto>> getAllByUsername(@RequestParam String username) {
        Response<DictionariesDto> response = new Response<>();
        try {
            response.setStatus(200);
            response.setList(dictionariesService.getAllByUsername(username));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/search")
    @CrossOrigin
    public ResponseEntity<Response<DictionariesDto>> search(@RequestParam String key) {
        Response<DictionariesDto> response = new Response<>();
        try {
            response.setStatus(200);
            response.setList(dictionariesService.search(key));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update")
    @CrossOrigin
    public ResponseEntity<Response<Object>> update(@RequestBody DictionariesDto dictionary) {
        Response<Object> response = new Response<>();
        try {
            if (dictionary.getName() != null) {
                Dictionaries d = dictionariesService.update(dictionary);
                response.setStatus(200);
                response.setMessage("Create new dictionary successfully.");
            } else {
                response.setStatus(500);
                response.setMessage("Dictionary name is required.");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
