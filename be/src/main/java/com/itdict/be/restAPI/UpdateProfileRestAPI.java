package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Roles;
import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.UserDto;
import com.itdict.be.service.RoleService;
import com.itdict.be.service.UpdateProfileService;
import com.itdict.be.service.UserService;
import org.hibernate.sql.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Calendar;

@RestController
@RequestMapping("api/updateProfile")
public class UpdateProfileRestAPI {
    @Autowired
    private UpdateProfileService updateProfileService;
    @GetMapping("/getUser")
    @CrossOrigin
    public ResponseEntity<Response<UserDto>> getUser(@RequestParam String username) {
        Response<UserDto> response = new Response<>();
        try {
            Users u = updateProfileService.getUser(username);
            response.setStatus(200);
            response.setData(new UserDto(u));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update")
    @CrossOrigin
    public ResponseEntity<Response<Object>> update(@RequestBody Users user) {
        Response<Object> response = new Response<>();
        try {
            Users existUser = updateProfileService.getUser(user.getUsername());
            if (existUser != null) {
                if (user.getUsername() != null) {
                    if (user.getEmail() != null || user.getPhone() != null) {
                        if (user.getFullname() != null) {
                            existUser.setEmail(user.getEmail());
                            existUser.setPhone(user.getPhone());
                            existUser.setFullname(user.getFullname());
                            existUser.setDob(user.getDob());
                            if(user.getJobId() != null){
                                existUser.setJobId(user.getJobId());
                            }
                            existUser.setUpdatedBy(user.getUsername());
                            existUser.setUpdated(new Date(System.currentTimeMillis()));
                            updateProfileService.save(existUser);
                            response.setStatus(200);
                            response.setMessage("Update profile successfully.");
                            response.setData(new UserDto(user.getUsername(),user.getFullname(),user.getRoleId()));
                        } else {
                            response.setStatus(500);
                            response.setMessage("Full name is required.");
                        }
                    } else {
                        response.setStatus(500);
                        response.setMessage("Phone or email is required.");
                    }
                } else {
                    response.setStatus(500);
                    response.setMessage("Username is required.");
                }
            } else {
                response.setStatus(500);
                response.setMessage("Username is not existed.");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
