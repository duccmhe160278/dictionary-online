package com.itdict.be.restAPI;

//import org.springframework.web.bind.annotation.RequestMapping;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Permissions;
import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.LoginInfoDto;
import com.itdict.be.entity.dto.MenuDto;
import com.itdict.be.entity.dto.UserDto;
import com.itdict.be.service.PermissionsService;
import com.itdict.be.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;

@RestController
@RequestMapping("api/authentication")
public class LoginRestAPI {
    @Autowired
    private UserService userService;
    @Autowired
    private PermissionsService permissionsService;

    @PostMapping("/login")
    @CrossOrigin
    public ResponseEntity<Response<LoginInfoDto>> login(@RequestBody Users user) {
        Response<LoginInfoDto> response = new Response<>();
        try {
            // get user
            Users u = userService.getByUsername(user.getUsername());

            if (u != null) {
                if (u.getActive() == true) {
                    if (user.getPassword().equals(u.getPassword())) {
                        // nhatbn update IT3
                        List<MenuDto> menu = permissionsService.getMenu(u.getRoleId());
                        LoginInfoDto returnData = new LoginInfoDto();
                        returnData.setMenu(menu);
                        returnData.setUsername(u.getUsername());
                        returnData.setFullname(u.getFullname());
                        returnData.setMenu(menu);
                        response.setData(returnData);
                        //----
                        response.setMessage("Login successfully.");
                        response.setStatus(200);
                        return new ResponseEntity<>(response, HttpStatus.OK);
                    } else {
                        response.setStatus(201);
                        response.setMessage("Password is incorrect. Please try again!");
                    }
                } else {
                    response.setStatus(201);
                    response.setMessage("Account is inactive. Please contact to mail [admin@gmail.com]!");
                }
            } else {
                response.setStatus(201);
                response.setMessage("Username is not exist. Please try again!");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/register")
    @CrossOrigin

    public ResponseEntity<Response<Object>> register(@RequestBody Users u) {
        Response<Object> response = new Response<>();
        try {
            Users existUser = userService.getByUsername(u.getUsername());
            if (existUser == null) {
                if (u.getUsername() != null) {
                    if (u.getEmail() != null || u.getPhone() != null) {
                        if (u.getFullname() != null) {
                            if (u.getPassword() != null) {
                                u.setActive(true);
                                u.setRoleId(2);
                                u.setCreated(new Date(System.currentTimeMillis()));
                                userService.save(u);
                                response.setStatus(200);
                                response.setMessage("Register successfully.");
                                response.setData(new UserDto(u.getUsername(),u.getFullname(),u.getRoleId()));
                            } else {
                                response.setStatus(500);
                                response.setMessage("Password is required.");
                            }
                        } else {
                            response.setStatus(500);
                            response.setMessage("Full name is required.");
                        }
                    } else {
                        response.setStatus(500);
                        response.setMessage("Phone or email is required.");
                    }
                } else {
                    response.setStatus(500);
                    response.setMessage("Username is required.");
                }
            } else {
                response.setStatus(500);
                response.setMessage("Username is existed.");
            }

//            userService.getLoginInfo(username, password);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
