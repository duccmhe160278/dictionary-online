package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Categories;
import com.itdict.be.entity.Jobs;
import com.itdict.be.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("api/category")
public class CategoryRestAPI {
    @Autowired
    private CategoryService categoryService;
    @GetMapping("getAll")
    @CrossOrigin
    public ResponseEntity<Response<Categories>> getAll() {
        Response<Categories> response = new Response<>();
        try{
            response.setStatus(200);
            response.setList(categoryService.getAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/create")
    @CrossOrigin
    public ResponseEntity<Response<Categories>> create(@RequestParam String name) {
        Response<Categories> response = new Response<>();
        try{
            Categories c = new Categories();
            c.setName(name);
            categoryService.create(c);
            response.setStatus(200);
            response.setMessage("Create successfully!");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    @CrossOrigin
    public ResponseEntity<Response<Categories>> create(@RequestBody Categories category) {
        Response<Categories> response = new Response<>();
        try{
            Categories c = categoryService.getByName(category.getName());
            if(c != null){
                response.setStatus(500);
                response.setMessage("Category is existed.");
            }else {
                category.setCreated(new Date());
                category.setCreatedBy(category.getCreatedBy());
                category.setUpdated(new Date());
                category.setCreatedBy(category.getUpdatedBy());
                categoryService.create(category);
                response.setStatus(200);
                response.setMessage("Create successfully!");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/delete")
    @CrossOrigin
    public ResponseEntity<Response<Object>> delete(@RequestParam String name) {
        Response<Object> response = new Response<>();
        try{
            Categories c = categoryService.getByName(name);
            if(c == null){
                response.setStatus(500);
                response.setMessage("Category is not existed.");
            }
            else {
                categoryService.delete(c.getId());
                response.setStatus(200);
                response.setMessage("Delete successfully!");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
