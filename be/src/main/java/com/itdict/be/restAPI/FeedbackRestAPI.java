package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.DictMapCateId;
import com.itdict.be.entity.Dictionaries;
import com.itdict.be.entity.DictsMapCates;
import com.itdict.be.entity.Feedbacks;
import com.itdict.be.entity.dto.FeedbackReportDto;
import com.itdict.be.service.DictionariesService;
import com.itdict.be.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/feedback/")
public class FeedbackRestAPI {
    @Autowired
    private FeedbackService feedbackService;
    @GetMapping("/get-list")
    @CrossOrigin
    public ResponseEntity<Response<Feedbacks>> getList(@RequestParam Integer rate) {
        Response<Feedbacks> response = new Response<>();
        try {
            response.setStatus(200);
            response.setList(feedbackService.getAllFeedbacks(rate));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get-report")
    @CrossOrigin
    public ResponseEntity<Response<FeedbackReportDto>> getReport() {
        Response<FeedbackReportDto> response = new Response<>();
        try {
            response.setStatus(200);
            response.setList(feedbackService.getReport());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping ("/send-feedback")
    @CrossOrigin
    public ResponseEntity<Response<Object>> sendFeedback(@RequestBody Feedbacks feedbacks) {
        Response<Object> response = new Response<>();
        try {
            if (feedbacks.getDescription() != null) {
                Feedbacks d = feedbackService.save(feedbacks);
                response.setStatus(200);
                response.setMessage("Create new dictionary successfully.");
            } else {
                response.setStatus(500);
                response.setMessage("Feedback description is required.");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
